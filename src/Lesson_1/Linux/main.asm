section .data
	HelloWorld db 'Hello world', 0xA, 0x0

section .text
	extern puts
	extern exit
	global main

main:
	push HelloWorld
	call puts
	mov eax, 0
	call exit