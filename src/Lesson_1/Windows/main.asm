section .data
	HelloWorld db 'Hello world', 0xA, 0x0

section .text
	extern _puts
	extern _exit
	global _main

_main:
	push HelloWorld
	call _puts
	mov eax, 0
	call _exit