section .data
	str01 db 'RESULT: %i', 0xA, 0
	day db ; день рождения
	month db ; месяц рождения
	year dw ; год рождения

section .bss
	result resd 1 ; результат вычислений

section .text
	extern printf
	extern exit
	global main

main:

	; ВАШ КОД ЗДЕСЬ
	
	push dword [result]
	push str01
	call printf

	mov eax, 0
	call exit
